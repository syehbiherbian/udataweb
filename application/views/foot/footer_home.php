<!-- Footer -->
<footer class="text-center">
    <div class="footer-above">
        <div class="container">
            <div class="row">
                <div class="footer-col col-md-4">
                    <h3>UData</h3>
                    <p></p>
                </div>
                <div class="footer-col col-md-4">
                    <h3>Tentang Kami</h3>
                    <ul class="list">

                    </ul>
                </div>
                <div class="footer-col col-md-4">
                    <h3>Kontak</h3>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-below">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    Copyright &copy; UData | Big Data Solutions
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
<div class="scroll-top page-scroll hidden-sm hidden-xs hidden-lg hidden-md">
    <a class="btn btn-primary" href="#page-top">
        <i class="fa fa-chevron-up"></i>
    </a>
</div>
<!-- jQuery -->
<script src="<?= base_url()?>assets/js/lib/jquery-2.2.0.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?= base_url()?>assets/js/lib/bootstrap.min.js"></script>

<!-- Plugin JavaScript -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>


<!-- Theme JavaScript -->
<script src="<?= base_url()?>assets/js/lib/freelancer.min.js"></script>

<!-- Materialize JS -->
<script src="<?= base_url()?>assets/js/lib/materialize.js"></script>


<script src="<?= base_url()?>assets/js/lib/jScrollPane.js"></script>

<script src="<?= base_url()?>assets/js/lib/jquery.filterizr.min.js"></script>

<script src="<?= base_url()?>assets/js/lib/script.js"></script>
</body>

</html>
