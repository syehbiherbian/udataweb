<?php $this->load->view('head/header_home'); ?>
<link href="<?= base_url()?>assets/css/components/client.css" rel="stylesheet">

<!-- Portfolio Grid Section -->
<section id="portfolio" class="content">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2>Klien Kami</h2>
                <hr class="star-primary">
            </div>
            <div class="client-sorting">
                <ul>
                    <li class="sort-active" data-filter="all">All</li>
                    <li data-filter="1">Aviation</li>
                    <li data-filter="2">Banking</li>
                    <li data-filter="3">Energy &amp; resource</li>
                    <li data-filter="4">Goverment</li>
                    <li data-filter="5">Insurance</li>
                    <li data-filter="6">Manufacturing &amp; Agribusiness</li>
                    <li data-filter="7">Media &amp; Communication</li>
                </ul>
            </div>
        </div>



        <div class="our-client-container">
            <div class="row">
                <div class="filtr-item col-md-3 col-centered" data-category="1" data-sort="value">
                    <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                        <img src="<?= base_url() ?>assets/img/klien/logo-garudaindo.png" alt="sample">
                    </a>
                </div>

                <div class="filtr-item col-md-3 col-centered" data-category="5" data-sort="value">
                    <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                        <img src="<?= base_url() ?>assets/img/klien/logo-commonweath.png" alt="sample">
                    </a>
                </div>
            
                <div class="filtr-item col-md-3 col-centered" data-category="2" data-sort="value">
                    <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                        <img src="<?= base_url() ?>assets/img/klien/logo-bni.png" alt="sample">
                    </a>
                </div>

                <div class="filtr-item col-md-3 col-centered" data-category="5" data-sort="value">
                    <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                        <img src="<?= base_url() ?>assets/img/klien/logo-equity.png" alt="sample">
                    </a>
                </div>
            
                <div class="filtr-item col-md-3 col-centered" data-category="6" data-sort="value">
                    <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                        <img src="<?= base_url() ?>assets/img/klien/logo-kimiafarma.png" alt="sample">
                    </a>
                </div>
            
                <div class="filtr-item col-md-3 col-centered" data-category="4" data-sort="value">
                    <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                     <img src="<?= base_url() ?>assets/img/klien/logo-kotabandung.png" alt="sample">
                    </a>
                </div>

                <div class="filtr-item col-md-3 col-centered" data-category="4" data-sort="value">
                    <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                        <img src="<?= base_url() ?>assets/img/klien/logo-kab-bandung.png" alt="sample">
                    </a>
                </div>
            
                <div class="filtr-item col-md-3 col-centered" data-category="4" data-sort="value">
                    <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                        <img src="<?= base_url() ?>assets/img/klien/logo-kotabogor.png" alt="sample">
                    </a>
                </div>
            
                <div class="filtr-item col-md-3 col-centered" data-category="1, 3" data-sort="value">
                    <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                        <img src="<?= base_url() ?>assets/img/klien/logo-bandungresik.png" alt="sample">
                    </a>
                </div>

                <div class="filtr-item col-md-3 col-centered" data-category="3" data-sort="value">
                    <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                        <img src="<?= base_url() ?>assets/img/klien/logo-pertamina.png" alt="sample">
                    </a>
                </div>
            
                <div class="filtr-item col-md-3 col-centered" data-category="5" data-sort="value">
                    <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                        <img src="<?= base_url() ?>assets/img/klien/admedika.png" alt="sample">
                    </a>
                </div>
            
                <div class="filtr-item col-md-3 col-centered" data-category="6" data-sort="value">
                    <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                        <img src="<?= base_url() ?>assets/img/klien/client-ptpn-x.png" alt="sample">
                    </a>
                </div>

                <div class="filtr-item col-md-3 col-centered" data-category="7" data-sort="value">
                    <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                        <img src="<?= base_url() ?>assets/img/klien/Client-telkomsel.png" alt="sample">
                    </a>
                </div>
                
                <div class="filtr-item col-md-3 col-centered" data-category="6" data-sort="value">
                    <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                        <img src="<?= base_url() ?>assets/img/klien/logo-domino.png" alt="sample">
                    </a>
                </div>
            </div>
        </div>

        <!--
        <div class="row">
            <div class="col-lg-12">
                <ul class="tabs">
                    <li class="tab"><a class="active" href="#test1">Test 1</a></li>
                    <li class="tab"><a href="#test2">Test 2</a></li>
                    <li class="tab"><a href="#test3">Test 3</a></li>
                    <li class="tab"><a href="#test4">Test 4</a></li>
                    <li class="tab"><a href="#test5">Test 5</a></li>
                    <li class="tab"><a href="#test6">Test 6</a></li>
                    <li class="tab"><a href="#test7">Test 7</a></li>
                    <li class="tab"><a href="#test8">Test 8</a></li>
                </ul>
            </div>

            <div id="test1" class="col-lg-12 scroll-pane">
                <div class="row">
                    <br>
                    <div class="col-sm-3 portfolio-item">
                        <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                            <div class="caption">
                                <div class="caption-content">
                                    <i class="fa fa-search-plus fa-3x"></i>
                                </div>
                            </div>
                            <img src="<?= base_url() ?>assets/img/klien/safe.png" class="img-responsive" alt="">
                        </a>
                    </div>

                    <div class="col-sm-3 portfolio-item">
                        <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                            <div class="caption">
                                <div class="caption-content">
                                    <i class="fa fa-search-plus fa-3x"></i>
                                </div>
                            </div>
                            <img src="<?= base_url() ?>assets/img/klien/safe.png" class="img-responsive" alt="">
                        </a>
                    </div>

                    <div class="col-sm-3 portfolio-item">
                        <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                            <div class="caption">
                                <div class="caption-content">
                                    <i class="fa fa-search-plus fa-3x"></i>
                                </div>
                            </div>
                            <img src="<?= base_url() ?>assets/img/klien/safe.png" class="img-responsive" alt="">
                        </a>
                    </div>

                    <div class="col-sm-3 portfolio-item">
                        <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                            <div class="caption">
                                <div class="caption-content">
                                    <i class="fa fa-search-plus fa-3x"></i>
                                </div>
                            </div>
                            <img src="<?= base_url() ?>assets/img/klien/safe.png" class="img-responsive" alt="">
                        </a>
                    </div>

                    <div class="col-sm-3 portfolio-item">
                        <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                            <div class="caption">
                                <div class="caption-content">
                                    <i class="fa fa-search-plus fa-3x"></i>
                                </div>
                            </div>
                            <img src="<?= base_url() ?>assets/img/klien/safe.png" class="img-responsive" alt="">
                        </a>
                    </div>

                    <div class="col-sm-3 portfolio-item">
                        <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                            <div class="caption">
                                <div class="caption-content">
                                    <i class="fa fa-search-plus fa-3x"></i>
                                </div>
                            </div>
                            <img src="<?= base_url() ?>assets/img/klien/safe.png" class="img-responsive" alt="">
                        </a>
                    </div>

                    <div class="col-sm-3 portfolio-item">
                        <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                            <div class="caption">
                                <div class="caption-content">
                                    <i class="fa fa-search-plus fa-3x"></i>
                                </div>
                            </div>
                            <img src="<?= base_url() ?>assets/img/klien/safe.png" class="img-responsive" alt="">
                        </a>
                    </div>

                    <div class="col-sm-3 portfolio-item">
                        <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                            <div class="caption">
                                <div class="caption-content">
                                    <i class="fa fa-search-plus fa-3x"></i>
                                </div>
                            </div>
                            <img src="<?= base_url() ?>assets/img/klien/safe.png" class="img-responsive" alt="">
                        </a>
                    </div>

                    <div class="col-sm-3 portfolio-item">
                        <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                            <div class="caption">
                                <div class="caption-content">
                                    <i class="fa fa-search-plus fa-3x"></i>
                                </div>
                            </div>
                            <img src="<?= base_url() ?>assets/img/klien/safe.png" class="img-responsive" alt="">
                        </a>
                    </div>

                    <div class="col-sm-3 portfolio-item">
                        <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                            <div class="caption">
                                <div class="caption-content">
                                    <i class="fa fa-search-plus fa-3x"></i>
                                </div>
                            </div>
                            <img src="<?= base_url() ?>assets/img/klien/safe.png" class="img-responsive" alt="">
                        </a>
                    </div>


                    <div class="col-sm-3 portfolio-item">
                        <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                            <div class="caption">
                                <div class="caption-content">
                                    <i class="fa fa-search-plus fa-3x"></i>
                                </div>
                            </div>
                            <img src="<?= base_url() ?>assets/img/klien/safe.png" class="img-responsive" alt="">
                        </a>
                    </div>


                    <div class="col-sm-3 portfolio-item">
                        <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                            <div class="caption">
                                <div class="caption-content">
                                    <i class="fa fa-search-plus fa-3x"></i>
                                </div>
                            </div>
                            <img src="<?= base_url() ?>assets/img/klien/safe.png" class="img-responsive" alt="">
                        </a>
                    </div>


                </div>

            </div>

            <div id="test2" class="col-lg-3 mybox">
                <p>Test 2</p>
            </div>
            <div id="test3" class="col s9 mybox">
                <p>Test 3</p>
            </div>
            <div id="test4" class="col s9 mybox">
                <p>Test 4</p>
            </div>
            <div id="test5" class="col s9 mybox">
                <p>Test 5</p>
            </div>
            <div id="test6" class="col s9 mybox">
                <p>Test 6</p>
            </div>
            <div id="test7" class="col s9 mybox">
                <p>Test 7</p>
            </div>
            <div id="test8" class="col s9 mybox">
                <p>Test 8</p>
            </div>
        </div>
        -->
    </div>
</section>

<div id="portfolioModal5" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-4">
                        <img src="<?= base_url() ?>assets/img/klien/safe.png" class="img-responsive" alt="">
                    </div>
                    <div class="col-lg-8">
                        <p>Ini nanti isinya testimoni dari pihak perusahaan terkenal klien telkom</p>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<?php $this->load->view('foot/footer_home'); ?>

<style>
    .scroll-pane {
        height: 500px;
        overflow: auto;
    }
</style>